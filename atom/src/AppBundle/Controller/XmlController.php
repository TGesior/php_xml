<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\XmlHandler;
use AppBundle\Entity\Xml;
use AppBundle\Entity\XmlAutor;
use AppBundle\Entity\XmlCounter;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class XmlController extends Controller
{
    
   
    /**
     * @Route("/xml", name="xml_index")
     */
    public function indexAction()
    {
        //Tworzenie obiektow
        $xmlCounter = new XmlCounter;
        $dataAuthor = new XmlAutor();
        $xmlContent = new XmlHandler();
        $xmlData    = new Xml();
        
        //Pobieranie danych z bazy danych
        $xml_content = $this->getDoctrine()
                ->getRepository('AppBundle:Xml')
                ->findAll();
        
        $xml_autor = $this->getDoctrine()
                ->getRepository('AppBundle:XmlAutor')
                ->findAll();
        
        $xml_counter = $this->getDoctrine()
                ->getRepository('AppBundle:XmlCounter')
                ->findAll();
        
        //Ostatni czas odswiezania danych w bazie danych
        foreach($xml_counter as $timer)
        {
            $last_update_time =  $timer->getTime();
        }
        
        //Sprawdzenie czy ostatni czas odswiezania danych jest wiekszy niz 24h
        if($xmlCounter->displayTime() - $xmlCounter->getTimeDif() > $last_update_time){
            
        //Ustawianie nowego czasu odswiezania danych
        $xmlCounter->setTime(time());
        $em = $this->getDoctrine()->getManager();
        $em->persist($xmlCounter);
        $em->flush();
        
        //Pobieranie danych z pliku xml
        $xmlData = $xmlContent->getData()->entry;
        $xmlNameSpaces = $XmlContent->setNamespaces(true);
        
        
        
        //Aktualizowanie rekordow w tabeli xml_autor
        $dataAuthor->setTitle($xmlContent->title);
        $dataAuthor->setIcon($xmlContent->icon); 
        $dataAuthor->setHref($xmlContent->link->attributes()->href); 
        $dataAuthor->setName($xmlContent->author->name);
        $dataAuthor->setUri($xmlContent->author->uri);
        $dataAuthor->setEmail($xmlContent->author->email);
        
        //Wykonanie zapytania do tabeli xml_autor
        $em = $this->getDoctrine()->getManager();        
        $em->persist($dataAuthor);        
        $em->flush();

     foreach($xmlContent as $xmlInfo)
     {
        $setData = new Xml();
        //Obsluga elementow z namespace 
        $nsXml = $xmlData->children($name['default']);
        
        //Aktualizowanie rekordow w tabeli xml
        $setData->setTitle($xmlInfo->title);
        $setData->setPublished($xmlinfo->published);
        $setData->setUpdated($xmlInfo->updated);
        $setData->setXmlId($xmlInfo->id); 
        $setData->setLinkRel($xmlInfo->link[0]['rel']); 
        $setData->setLinkType($xmlInfo->link[0]['type']); 
        $setData->setLinkHref($xmlInfo->link[0]['href']); 
        $setData->setSecondLinkRel($xmlInfo->link[1]['rel']); 
        $setData->setSecondLinkType($xmlInfo->link[1]['type']); 
        $setData->setSecondLinkHref($xmlInfo->link[1]['href']);         
        $setData->setCategoryTerm($xmlInfo->category->attributes()->term); 
        $setData->setCategoryLabel($xmlInfo->category->attributes()->label);
         
        $setData->setFinalTeaserDate($nsXml->finalTeaserDate); 
        $setData->setNewsImage($nsXml->newsImage);          
        $setData->setContent($xmlContent->cleanResult(json_encode($xmlInfo->content->div)));
        //Wykonanie zapytania do bazy danych
        $em = $this->getDoctrine()->getManager();       
        $em->persist($setData);
        
        $em->flush();        
        $em->clear();
         
     }
        }
    return $this->render('xml/index.html.twig', array('result' => $xml_content, 'author' => $xml_autor));
 
    }
    
     /**
     * @Route("/add", name="xml_create")
     */
     public function addAction(Request $request)
    {
         $xmlContent = new Xml;
         
         $form = $this->createFormBuilder($xmlContent)
                 ->add('title',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('published',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('updated',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('xmlid',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('linkrel',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('linktype',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('linkhref',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('secondlinkrel',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('secondlinktype',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('secondlinkhref',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('categoryterm',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('categorylabel',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('finalteaserdate',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('newsimage',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('content',TextAreaType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('submit',SubmitType::class, array('label' => 'Stworz News', 'attr' => array('class' => 'btn btn-success',
                     'style' => 'margin-bottom:5px')))
                 ->getForm();
         
         $form->handleRequest($request);
         
         if($form->isSubmitted() && $form->isValid())
         {
             $title           = $form['title']->getData();
             $published       = $form['published']->getData();
             $updated         = $form['updated']->getData();
             $xmlId           = $form['xmlid']->getData();
             $linkRel         = $form['linkrel']->getData();
             $linkType        = $form['linktype']->getData();
             $linkHref        = $form['linkhref']->getData();
             $secondLinkRel   = $form['secondlinkrel']->getData();
             $secondLinkType  = $form['secondlinktype']->getData();
             $secondLinkHref  = $form['secondlinkhref']->getData();
             $categoryTerm    = $form['categoryterm']->getData();
             $categoryLabel   = $form['categorylabel']->getData();
             $finalTeaserDate = $form['finalteaserdate']->getData();
             $newsImage       = $form['newsimage']->getData();
             $content         = $form['content']->getData();
             
             
             
            $xmlContent->setTitle($title);
            $xmlContent->setPublished($published);
            $xmlContent->setUpdated($updated);
            $xmlContent->setXmlId($xmlId);
            $xmlContent->setLinkRel($linkRel); 
            $xmlContent->setLinkType($linkType); 
            $xmlContent->setLinkHref($linkHref); 
            $xmlContent->setSecondLinkRel($secondLinkRel); 
            $xmlContent->setSecondLinkType($secondLinkType); 
            $xmlContent->setSecondLinkHref($secondLinkHref); 
            $xmlContent->setCategoryTerm($categoryTerm); 
            $xmlContent->setCategoryLabel($categoryLabel);
            $xmlContent->setFinalTeaserDate($finalTeaserDate); 
            $xmlContent->setNewsImage($newsImage); 
            $xmlContent->setContent($content);
             
             
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($xmlContent);
            $em->flush();           
            $this->addFlash(
                    'notice',
                    'Dodano News');
             
            return $this->redirectToRoute('xml_index');
             
         }

        return $this->render('xml/add.html.twig', array('form' => $form->createView()));
    }
    
    
    /**
     * @Route("/edit/{id}", name="xml_edit")
     */
    public function editAction($id, Request $request)
    {
        $xmlContent = $this->getDoctrine()
                ->getRepository('AppBundle:Xml')
                ->find($id);
        
        $xmlContent->setTitle($xmlContent->getTitle());
        $xmlContent->setPublished($xmlContent->getPublished());
        $xmlContent->setUpdated($xmlContent->getUpdated());
        $xmlContent->setXmlId($xmlContent->getXmlId());
        $xmlContent->setLinkRel($xmlContent->getLinkRel()); 
        $xmlContent->setLinkType($xmlContent->getLinkType()); 
        $xmlContent->setLinkHref($xmlContent->getLinkHref()); 
        $xmlContent->setSecondLinkRel($xmlContent->getSecondLinkRel()); 
        $xmlContent->setSecondLinkType($xmlContent->getSecondLinkType()); 
        $xmlContent->setSecondLinkHref($xmlContent->getSecondLinkHref()); 
        $xmlContent->setCategoryTerm($xmlContent->getCategoryTerm()); 
        $xmlContent->setCategoryLabel($xmlContent->getCategoryLabel());
        $xmlContent->setFinalTeaserDate($xmlContent->getFinalTeaserDate()); 
        $xmlContent->setNewsImage($xmlContent->getNewsImage()); 
        $xmlContent->setContent($xmlContent->getContent());
        
        
         
         $form = $this->createFormBuilder($xmlContent)
                 ->add('title',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('published',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('updated',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('xmlid',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('linkrel',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('linktype',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('linkhref',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('secondlinkrel',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('secondlinktype',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('secondlinkhref',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('categoryterm',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('categorylabel',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('finalteaserdate',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('newsimage',TextType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('content',TextAreaType::class, array('attr' => array('class' => 'form-control',
                     'style' => 'margin-bottom:5px')))
                 ->add('submit',SubmitType::class, array('label' => 'Zaaktualizuj News', 'attr' => array('class' => 'btn btn-success',
                     'style' => 'margin-bottom:5px')))
                 ->getForm();
         
         $form->handleRequest($request);
         
         if($form->isSubmitted() && $form->isValid())
         {
             $title           = $form['title']->getData();
             $published       = $form['published']->getData();
             $updated         = $form['updated']->getData();
             $xmlId           = $form['xmlid']->getData();
             $linkRel         = $form['linkrel']->getData();
             $linkType        = $form['linktype']->getData();
             $linkHref        = $form['linkhref']->getData();
             $secondLinkRel   = $form['secondlinkrel']->getData();
             $secondLinkType  = $form['secondlinktype']->getData();
             $secondLinkHref  = $form['secondlinkhref']->getData();
             $categoryTerm    = $form['categoryterm']->getData();
             $categoryLabel   = $form['categorylabel']->getData();
             $finalTeaserDate = $form['finalteaserdate']->getData();
             $newsImage       = $form['newsimage']->getData();
             $content         = $form['content']->getData();
             
            $em = $this->getDoctrine()->getManager();
            $xml = $em->getRepository('AppBundle:Xml')->find($id);
             
            $xmlContent->setTitle($title);
            $xmlContent->setPublished($published);
            $xmlContent->setUpdated($updated);
            $xmlContent->setXmlId($xmlId);
            $xmlContent->setLinkRel($linkRel); 
            $xmlContent->setLinkType($linkType); 
            $xmlContent->setLinkHref($linkHref); 
            $xmlContent->setSecondLinkRel($secondLinkRel); 
            $xmlContent->setSecondLinkType($secondLinkType); 
            $xmlContent->setSecondLinkHref($secondLinkHref); 
            $xmlContent->setCategoryTerm($categoryTerm); 
            $xmlContent->setCategoryLabel($categoryLabel);
            $xmlContent->setFinalTeaserDate($finalTeaserDate); 
            $xmlContent->setNewsImage($newsImage); 
            $xmlContent->setContent($content);
             
            $em->flush();           
            $this->addFlash(
                    'notice',
                    'Zedytowno news');
             
            return $this->redirectToRoute('xml_index');
             
         }

        return $this->render('xml/edit.html.twig', array(
            'xml' => $xmlContent, 'form' => $form->createView()));
    }
    

    /**
     * @Route("/delete/{id}", name="xml_delete")
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $xml = $em->getRepository('AppBundle:Xml')->find($id);
        
        $em->remove($xml);
        $em->flush();
        
        $this->addFlash(
                    'notice',
                    'Usunieto news');
             
            return $this->redirectToRoute('xml_index');       
    }
    
}
