<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XmlCounter
 *
 * @ORM\Table(name="xml_counter")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\XmlCounterRepository")
 */
class XmlCounter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="integer", nullable=true)
     */
    private $time;


    /**
     * Get id
     *
     * @return int
     */
    
    /**
     * Roznica czasu
     * @var type 
     */
    private $timeDif = 60 * 60 *24;
    
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return XmlCounter
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }
    
    /**
     * 
     * @return int
     */
    public function getTimeDif()
    {
        return $this->timeDif;
    }
    
    /**
     * 
     * @return int
     */
    public function displayTime()
    {
        return time();
    }
}

