<?php

namespace AppBundle\Entity;

class XmlHandler
{
    
    /**
     * Zmienna zawiera adres pliku XML
     * @var type plik XML
     */
    private $file;
    
    /**
     * Zmienna zawiera dane pliku XML
     * @var type object SimpleXml
     */
    private $data;

    
    
    public function __construct()
    {
        $this->file = 'http://php.net/feed.atom';
        
        $this->data = simplexml_load_file($this->file);
        
    }
    /**
     * 
     * @return type object SimpleXml
     */
    public function getData()
    {
        return $this->data;
    }
    
    public function setNamespaces($bool = true)
    {
        
        if($bool == true)
        {
           return $this->data->getNamespaces(true); 
        }
        else
        {
           return $this->data->getNamespaces(); 
        }
    }
    
       
    public function cleanResult($string)
    {
        $elements = array( '{"p":["', '\n' , ']}' , '{"a"');
        
             $string = str_replace($elements[0], '', $string);
             $string = str_replace($elements[1], '', $string);
             $string = str_replace($elements[2], '', $string);
             $string = str_replace($elements[3], '', $string);
             
            return $string;
    }
    
}

