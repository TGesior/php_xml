<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xml
 *
 * @ORM\Table(name="xml")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\XmlRepository")
 */
class Xml
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="link_href", type="string", length=255, nullable=true)
     */
    private $linkHref;

    /**
     * @var string
     *
     * @ORM\Column(name="link_rel", type="string", length=255, nullable=true)
     */
    private $linkRel;

    /**
     * @var string
     *
     * @ORM\Column(name="link_type", type="string", length=255, nullable=true)
     */
    private $linkType;

    /**
     * @var string
     *
     * @ORM\Column(name="xml_id", type="string", length=255, nullable=true)
     */
    private $xmlId;

    /**
     * @var string
     *
     * @ORM\Column(name="category_term", type="string", length=255, nullable=true)
     */
    private $categoryTerm;

    /**
     * @var string
     *
     * @ORM\Column(name="category_label", type="string", length=255, nullable=true)
     */
    private $categoryLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="finalTeaserDate", type="string", length=255, nullable=true)
     */
    private $finalTeaserDate;

    /**
     * @var string
     *
     * @ORM\Column(name="newsImage", type="string", length=255, nullable=true)
     */
    private $newsImage;

    /**
     * @var string
     *
     * @ORM\Column(name="published", type="string", length=255, nullable=true)
     */
    private $published;

    /**
     * @var string
     *
     * @ORM\Column(name="updated", type="string", length=255, nullable=true)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="second_link_href", type="string", length=255, nullable=true)
     */
    private $secondLinkHref;
    
    /**
     * @var string
     *
     * @ORM\Column(name="second_link_rel", type="string", length=255, nullable=true)
     */
    private $secondLinkRel;
    
     /**
     * @var string
     *
     * @ORM\Column(name="second_link_type", type="string", length=255, nullable=true)
     */
    private $secondLinkType;
    
        /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;
    
    
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Xml
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set linkHref
     *
     * @param string $linkHref
     *
     * @return Xml
     */
    public function setLinkHref($linkHref)
    {
        $this->linkHref = $linkHref;

        return $this;
    }

    /**
     * Get linkHref
     *
     * @return string
     */
    public function getLinkHref()
    {
        return $this->linkHref;
    }

    /**
     * Set linkRel
     *
     * @param string $linkRel
     *
     * @return Xml
     */
    public function setLinkRel($linkRel)
    {
        $this->linkRel = $linkRel;

        return $this;
    }

    /**
     * Get linkRel
     *
     * @return string
     */
    public function getLinkRel()
    {
        return $this->linkRel;
    }

    /**
     * Set linkType
     *
     * @param string $linkType
     *
     * @return Xml
     */
    public function setLinkType($linkType)
    {
        $this->linkType = $linkType;

        return $this;
    }

    /**
     * Get linkType
     *
     * @return string
     */
    public function getLinkType()
    {
        return $this->linkType;
    }

    /**
     * Set xmlId
     *
     * @param integer $xmlId
     *
     * @return Xml
     */
    public function setXmlId($xmlId)
    {
        $this->xmlId = $xmlId;

        return $this;
    }

    /**
     * Get xmlId
     *
     * @return int
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * Set categoryTerm
     *
     * @param string $categoryTerm
     *
     * @return Xml
     */
    public function setCategoryTerm($categoryTerm)
    {
        $this->categoryTerm = $categoryTerm;

        return $this;
    }

    /**
     * Get categoryTerm
     *
     * @return string
     */
    public function getCategoryTerm()
    {
        return $this->categoryTerm;
    }

    /**
     * Set categoryLabel
     *
     * @param string $categoryLabel
     *
     * @return Xml
     */
    public function setCategoryLabel($categoryLabel)
    {
        $this->categoryLabel = $categoryLabel;

        return $this;
    }

    /**
     * Get categoryLabel
     *
     * @return string
     */
    public function getCategoryLabel()
    {
        return $this->categoryLabel;
    }

    /**
     * Set finalTeaserDate
     *
     * @param string $finalTeaserDate
     *
     * @return Xml
     */
    public function setFinalTeaserDate($finalTeaserDate)
    {
        $this->finalTeaserDate = $finalTeaserDate;

        return $this;
    }

    /**
     * Get finalTeaserDate
     *
     * @return string
     */
    public function getFinalTeaserDate()
    {
        return $this->finalTeaserDate;
    }

    /**
     * Set newsImage
     *
     * @param string $newsImage
     *
     * @return Xml
     */
    public function setNewsImage($newsImage)
    {
        $this->newsImage = $newsImage;

        return $this;
    }

    /**
     * Get newsImage
     *
     * @return string
     */
    public function getNewsImage()
    {
        return $this->newsImage;
    }

    /**
     * Set published
     *
     * @param string $published
     *
     * @return Xml
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return string
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set updated
     *
     * @param string $updated
     *
     * @return Xml
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return string
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    
    /**
     * Set secondLinkHref
     *
     * @param string $secondLinkHref
     *
     * @return Xml
     */
    public function setSecondLinkHref($secondLinkHref)
    {
        $this->secondLinkHref = $secondLinkHref;

        return $this;
    }

    /**
     * Get secondLinkHref
     *
     * @return string
     */
    public function getSecondLinkHref()
    {
        return $this->secondLinkHref;
    }
    
    /**
     * Set secondLinkRel
     *
     * @param string $secondLinkRel
     *
     * @return Xml
     */
    public function setSecondLinkRel($secondLinkRel)
    {
        $this->secondLinkRel = $secondLinkRel;

        return $this;
    }

    /**
     * Get secondLinkRel
     *
     * @return string
     */
    public function getSecondLinkRel()
    {
        return $this->secondLinkRel;
    }
    
    /**
     * Set secondLinkType
     *
     * @param string $secondLinkType
     *
     * @return Xml
     */
    public function setSecondLinkType($secondLinkType)
    {
        $this->secondLinkType = $secondLinkType;

        return $this;
    }

    /**
     * Get secondLinkType
     *
     * @return string
     */
    public function getSecondLinkType()
    {
        return $this->secondLinkType;
    }
    
    /**
     * Set content
     *
     * @param string $content
     *
     * @return Xml
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}

